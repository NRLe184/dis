from ICommand import ICommand

class Move_up_command(ICommand):
    def __init__(self, elevator_object):
        self.elevator_object = elevator_object
    
    def execute_command(self):
        if (self.elevator_object.doors_is_opened):
            raise Exception("Лифт не может двигаться с открытыми дверями")
        self.elevator_object.floor += 1
        self.elevator_object.movements_history[self.elevator_object.id].append(f' ---> {self.elevator_object.floor}')
        if (self.elevator_object.floor > self.elevator_object.max_floor):
            raise Exception("Лифт не может подняться выше последнего этажа")
    
class Move_down_command(ICommand):
    def __init__(self, elevator_object):
        self.elevator_object = elevator_object
    
    def execute_command(self):
        if (self.elevator_object.doors_is_opened):
            raise Exception("Лифт не может двигаться с открытыми дверями")
        self.elevator_object.floor -= 1
        self.elevator_object.movements_history[self.elevator_object.id].append(f' ---> {self.elevator_object.floor}')
        if (self.elevator_object.floor < 1):
            raise Exception("Лифт не может опускаться ниже первого этажа")
        
class Open_doors_command(ICommand):
    def __init__(self, elevator_object):
        self.elevator_object = elevator_object
    
    def execute_command(self):
        if (self.elevator_object.doors_is_opened):
            raise Exception("Двери уже открыты")
        self.elevator_object.doors_is_opened = True
        self.elevator_object.movements_history[self.elevator_object.id].append(f'OPEN_DOORS')

class Close_doors_command(ICommand):
    def __init__(self, elevator_object):
        self.elevator_object = elevator_object
    
    def execute_command(self):
        if (not self.elevator_object.doors_is_opened):
            raise Exception("Двери уже закрыты")
        self.elevator_object.doors_is_opened = False
        self.elevator_object.movements_history[self.elevator_object.id].append(f'CLOSE_DOORS')

class DFA_command_process(ICommand):
    def __init__(self, state_machine, inputs):
        self.state_machine = state_machine
        self.inputs = inputs
        
    def execute_command(self):
        if (len(self.inputs) == 0):
            raise Exception("ДКА перешёл в терминальное состояние и завершил свою работу")
        new_input = self.inputs.pop(0)
        self.inputs.insert(0, self.state_machine.get_input())
        self.state_machine.set_input(new_input)
        self.inputs.pop(0)

