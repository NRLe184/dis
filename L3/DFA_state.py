from collections import defaultdict


class DFA_state():
    def __init__(self, input_symbols, transition_function):
        if input_symbols == None:
            self.input_symbols = defaultdict()
        else:
            self.input_symbols = input_symbols
        if transition_function == None:
            self.transition_function = {}
        else:
            self.transition_function = transition_function