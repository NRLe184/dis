from elevator import Elevator
from DFA import DFA
from generator_states import *
from DFA_state import DFA_state
import json

states = {}

with open('DFA_states.json', 'r') as file:
    DFA_states = json.loads(file.read())
    for key, value in DFA_states.items():
        DFA_states[key] = DFA_state(**value)

inputs = [
    '(5, 1)',
    '(1, 6)',
    '(0, 0)',
    '(6, 2)',
    '(2, 3)',
    '(0, 0)',
    '(1, 4)',
    '(4, 5)',
]

elevators = [Elevator(3, 6), Elevator(1, 6)]

DFA = DFA(elevators, DFA_states, 'START_STATE', inputs)

print('----ДКА начал свою работу----')

while True:
    try:
        DFA.DFA_commands_execute()
    except Exception as exception:
        print(f'----ДКА закончил свою работу---- по причине: {exception}')
        print('История операций каждого лифта:')
        for elevator in elevators:
            for k, v in elevator.movements_history.items():
                print(f'\t{k}: {v}')
        break
