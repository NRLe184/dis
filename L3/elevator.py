"""
Лабораторная работа №3 Разработка ДКА
Цель:
Научиться разрабатывать решать задачи с помощью ДКА
Смысл:
from collections import defaultdict


class Elevator():
    def __init__(self, id, max_floor):
        self.id = id
        self.max_floor = max_floor
        self.floor = 1
        self.doors_is_opened = False
        self.movements_history = defaultdict(list)