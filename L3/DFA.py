from elevators_commands import *

class DFA():
    def __init__(self, elevators, states, start_state, inputs):
        self.states = states
        self.now_state = start_state
        self.now_input = 'START_STATE'
        self.input_symbols = {
            'process_dfa_command': DFA_command_process(self, inputs),
        }
        for i, elevator in enumerate(elevators):
            self.input_symbols[f'ЛИФТ{i}_ВВЕРХ'] = Move_up_command(elevator)
            self.input_symbols[f'ЛИФТ{i}_ВНИЗ'] = Move_down_command(elevator)
            self.input_symbols[f'ЛИФТ{i}_ОТКР'] = Open_doors_command(elevator)
            self.input_symbols[f'ЛИФТ{i}_ЗАКР'] = Close_doors_command(elevator)

    def DFA_commands_execute(self):
        commands = self.states[self.now_state].input_symbols[self.now_input]
        self.now_state = self.states[self.now_state].transition_function[self.now_input]

        for command in commands: #как решить проблему с разными инпутами? Паттерн Command
            self.input_symbols[command].execute_command()

    def set_state(self, state_id):
        self.now_state = state_id

    def set_input(self, new_input):
        self.now_input = new_input

    def get_state(self):
        return self.now_state

    def get_input(self):
        return self.now_input