class Exceptions_raiser:
  def __init__(self, exc_handler):
    self.exc_handler = exc_handler

  def except_type_error(self):
    try:#TypeError
      result = 42 - [4]
      print(result)
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_key_error(self):
    try:#KeyError
      dict_data = {}
      dict_data["key"]
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_out_of_range(self):#out of range
    try:
      list_data = [15, 21, '2', '14']
      list_data[8]
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_attr_error(self):#Attribute Error
    try:
      list.unknown_attribute
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_file_not_exists(self):
    try:#File Not Exists
      with open("unknown.txt") as file:
        file.read()
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_division_by_zero(self):
      try:#Division by Zero
        5 / 0
      except Exception as e:
        exception_data = self.exc_handler.get_exception_info(e)
        self.exc_handler.print_exception_message(exception_data)

  def except_value_error(self): #value error, отсутствует в словаре exceptions
    import math
    try:
      math.sqrt(-2)
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)

  def except_name_error(self): #name error, отсутствует в словаре exceptions
    try:
      print(not_defined_var)
    except Exception as e:
      exception_data = self.exc_handler.get_exception_info(e)
      self.exc_handler.print_exception_message(exception_data)