import traceback
import sys

class Exception_handler:
  def __init__(self, exceptions):
    self.exceptions = exceptions

class Exception_handler:
  def __init__(self, exceptions):
    self.exceptions = exceptions

  def _get_exception_stack_call(self):
    stack_str = "".join(traceback.format_exception(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2]))
    return stack_str

  def get_exception_info(self, exception):
    exception_data = {}
    traceback_tb = traceback.extract_tb(exception.__traceback__)
    exception_reason = traceback.format_exception(exception)[2].split(":", 1)[1]
    exception_data["Стек вызовов"] = self._get_exception_stack_call()
    exception_data["Файл"] = traceback_tb[-1].filename
    exception_data["Номер строки"] = traceback_tb[-1].lineno
    exception_data["Код, вызвавший исключение"] = traceback.format_exc().splitlines()[-2]
    exception_data["Тип исключения"] = sys.exc_info()[0]
    exception_data["Причина исключения"] = exception_reason
    return exception_data

  def print_exception_message(self, exception_data):
    exception_type = exception_data['Тип исключения']
    exception_reason = exception_data['Причина исключения']
    exception_message = self.exceptions.get(exception_type)
    for key, value in exception_data.items():
      print(f"{key} -- {value}")
    if exception_message:
      print(f"Сообщение об исключении: {exception_message} {exception_reason}")
    else:
      print("Сообщение об исключении:", exception_reason)
      exception_message = str(exception_type) + ' ' + exception_reason
      self.exceptions[exception_type] = exception_message

